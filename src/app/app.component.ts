import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { Observable } from 'rxjs/';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  projectForm: FormGroup;
  forbiddenProjectName = 'test';

  ngOnInit () {
    this.projectForm = new FormGroup({
      'project': new FormControl(null, [Validators.required], this.forbiddenProjectsAsync.bind(this)),
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'status': new FormControl('stable')
    });
  }

  onSubmit() {
    console.log(this.projectForm);
  }

  forbiddenProjects(control: FormControl): {[s: string]: boolean} {
    if (control.value.toLowerCase() === this.forbiddenProjectName) {
      return {'forbiddenProjectName': true};
    }
    return null;
  }

  forbiddenProjectsAsync(control: FormControl): Promise<any> | Observable<any> {
    const promise = new Promise((resolve, reject) => {
      setTimeout(() => {
          if (control.value.toLowerCase() === this.forbiddenProjectName) {
            resolve({'forbiddenProjectName': true});
          } else {
            resolve(null);
          }
      }, 1500);
    });
    return promise;
  }
}
